import logging
import random
import string
from os import environ
from typing import Iterator

from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

from const import INDICES

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

DOCUMENTS_PER_INDEX = 100

es = Elasticsearch([environ.get("ES_HOST", "elasticsearch:9200")])


def random_word(word_length=10) -> str:
    return "".join(
        map(lambda _: random.choice(string.ascii_letters), range(word_length))
    )


def initial_data(index: str, doc_count=DOCUMENTS_PER_INDEX) -> Iterator[dict]:
    yield from map(
        lambda _: {
            "_index": index,
            "random_value": random_word(word_length=random.randint(3, 15)),
        },
        range(doc_count),
    )


def indices_data(indices: Iterator[str]) -> Iterator[dict]:
    for i in indices:
        yield from initial_data(index=i)


def make_indices():
    logger.info("Initializing indices")
    bulk(es, indices_data(INDICES))


if __name__ == "__main__":
    logger.info("Start")
    make_indices()
    logger.info("Done")
