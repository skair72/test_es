import logging
from os import environ

from elasticsearch import Elasticsearch

from const import RESULTING_INDEX

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

es = Elasticsearch([environ.get("ES_HOST", "elasticsearch:9200")])


def make_reindex():
    logger.info("Reindex task starts")
    return es.reindex(
        body={"source": {"index": "project_*"}, "dest": {"index": RESULTING_INDEX}},
        refresh=True,
    )


def add_last_updated():
    logger.info("Add last_updated task starts")
    es.update_by_query(
        RESULTING_INDEX,
        body={
            "script": {
                "source": "ctx._source.last_updated = new Date()",
                "lang": "painless",
            },
            "query": {"match_all": {}},
        },
        wait_for_completion=False,
    )


if __name__ == "__main__":
    logger.info("Start")
    make_reindex()
    add_last_updated()
    logger.info("Done.")
