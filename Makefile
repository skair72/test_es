.PHONY = elastic init_data reindex_and_update cleanup

elastic:
	docker-compose up -d --force-recreate elasticsearch

init_data:
	docker build -t test_es:latest .
	docker run --rm --env-file .env --network test_es test_es:latest python init.py

reindex_and_update:
	docker build -t test_es:latest .
	docker run --rm --env-file .env --network test_es test_es:latest python reindex_and_update.py

cleanup:
	docker rmi test_es:latest | true
	docker-compose down --rmi local -v