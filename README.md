## 

## How to use

#### Elasticsearch
For running elasticsearch inside docker
```shell script
make elastic
```
Or if elasticsearch running in a different place just rename ```.env.default``` file to ```.env``` and change ```ES_HOST``` value

#### Scripts
Running python script for initial data ingestion and indices creation
```shell script
make init_data
```
Running python script with reindex and update functionality
```shell script
make reindex_and_update
```

#### Cleanup
For remove created docker containers and etc run
```shell script
make cleanup
```
